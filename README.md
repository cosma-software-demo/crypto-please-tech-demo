# Clean Architecture Tech Demo

This is a Tech Demo of how to implement Clean Architecture in Flutter.

Libraries used:

 - flutter_bloc
 - freezed
 - json_serializable
 - dio + retrofit
 - injectable
 - get_it
