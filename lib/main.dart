import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart' as injectable;
import 'package:injectable/injectable.dart';
import 'core/dependency_injection/injector.dart';

import 'features/currencies/presentation/pages/currencies_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await configureDependencies(injectable.prod);

  runApp(getIt.get<MyApp>());
}

@Injectable()
class MyApp extends StatelessWidget {
  const MyApp() : super(key: null);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Crypto Please Tech Demo',
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      home: getIt.get<CurrenciesPage>(),
    );
  }
}
