// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currency_page_ui_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$CurrencyPageUiModelTearOff {
  const _$CurrencyPageUiModelTearOff();

  _CurrencyPageUiModel fact(List<Currency> currencies) {
    return _CurrencyPageUiModel(
      currencies,
    );
  }
}

/// @nodoc
const $CurrencyPageUiModel = _$CurrencyPageUiModelTearOff();

/// @nodoc
mixin _$CurrencyPageUiModel {
  List<Currency> get currencies => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Currency> currencies) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Currency> currencies)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Currency> currencies)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrencyPageUiModel value) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrencyPageUiModel value)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrencyPageUiModel value)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $CurrencyPageUiModelCopyWith<CurrencyPageUiModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyPageUiModelCopyWith<$Res> {
  factory $CurrencyPageUiModelCopyWith(
          CurrencyPageUiModel value, $Res Function(CurrencyPageUiModel) then) =
      _$CurrencyPageUiModelCopyWithImpl<$Res>;
  $Res call({List<Currency> currencies});
}

/// @nodoc
class _$CurrencyPageUiModelCopyWithImpl<$Res>
    implements $CurrencyPageUiModelCopyWith<$Res> {
  _$CurrencyPageUiModelCopyWithImpl(this._value, this._then);

  final CurrencyPageUiModel _value;
  // ignore: unused_field
  final $Res Function(CurrencyPageUiModel) _then;

  @override
  $Res call({
    Object? currencies = freezed,
  }) {
    return _then(_value.copyWith(
      currencies: currencies == freezed
          ? _value.currencies
          : currencies // ignore: cast_nullable_to_non_nullable
              as List<Currency>,
    ));
  }
}

/// @nodoc
abstract class _$CurrencyPageUiModelCopyWith<$Res>
    implements $CurrencyPageUiModelCopyWith<$Res> {
  factory _$CurrencyPageUiModelCopyWith(_CurrencyPageUiModel value,
          $Res Function(_CurrencyPageUiModel) then) =
      __$CurrencyPageUiModelCopyWithImpl<$Res>;
  @override
  $Res call({List<Currency> currencies});
}

/// @nodoc
class __$CurrencyPageUiModelCopyWithImpl<$Res>
    extends _$CurrencyPageUiModelCopyWithImpl<$Res>
    implements _$CurrencyPageUiModelCopyWith<$Res> {
  __$CurrencyPageUiModelCopyWithImpl(
      _CurrencyPageUiModel _value, $Res Function(_CurrencyPageUiModel) _then)
      : super(_value, (v) => _then(v as _CurrencyPageUiModel));

  @override
  _CurrencyPageUiModel get _value => super._value as _CurrencyPageUiModel;

  @override
  $Res call({
    Object? currencies = freezed,
  }) {
    return _then(_CurrencyPageUiModel(
      currencies == freezed
          ? _value.currencies
          : currencies // ignore: cast_nullable_to_non_nullable
              as List<Currency>,
    ));
  }
}

/// @nodoc

class _$_CurrencyPageUiModel implements _CurrencyPageUiModel {
  _$_CurrencyPageUiModel(this.currencies);

  @override
  final List<Currency> currencies;

  @override
  String toString() {
    return 'CurrencyPageUiModel.fact(currencies: $currencies)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CurrencyPageUiModel &&
            const DeepCollectionEquality()
                .equals(other.currencies, currencies));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(currencies));

  @JsonKey(ignore: true)
  @override
  _$CurrencyPageUiModelCopyWith<_CurrencyPageUiModel> get copyWith =>
      __$CurrencyPageUiModelCopyWithImpl<_CurrencyPageUiModel>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(List<Currency> currencies) fact,
  }) {
    return fact(currencies);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(List<Currency> currencies)? fact,
  }) {
    return fact?.call(currencies);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(List<Currency> currencies)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(currencies);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrencyPageUiModel value) fact,
  }) {
    return fact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrencyPageUiModel value)? fact,
  }) {
    return fact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrencyPageUiModel value)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(this);
    }
    return orElse();
  }
}

abstract class _CurrencyPageUiModel implements CurrencyPageUiModel {
  factory _CurrencyPageUiModel(List<Currency> currencies) =
      _$_CurrencyPageUiModel;

  @override
  List<Currency> get currencies;
  @override
  @JsonKey(ignore: true)
  _$CurrencyPageUiModelCopyWith<_CurrencyPageUiModel> get copyWith =>
      throw _privateConstructorUsedError;
}
