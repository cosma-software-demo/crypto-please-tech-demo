import 'package:cryptoplease_tech_demo/core/move_to_background_widget.dart';
import 'package:cryptoplease_tech_demo/features/currencies/presentation/cubits/currencies_page_cubit.dart';
import 'package:cryptoplease_tech_demo/features/currencies/presentation/cubits/currencies_page_states.dart';
import 'package:cryptoplease_tech_demo/features/exchange/presentation/pages/exchange_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class CurrenciesPage extends StatefulWidget {
  final CurrenciesPageCubit cubit;

  const CurrenciesPage(this.cubit) : super(key: null);

  @override
  _CurrenciesPageState createState() => _CurrenciesPageState();
}

class _CurrenciesPageState extends State<CurrenciesPage> {
  @override
  Widget build(BuildContext context) => MoveToBackGroundWidget(
        child: BlocConsumer<CurrenciesPageCubit, CurrencyPageState>(
          bloc: widget.cubit,
          listener: listener,
          builder: builder,
          buildWhen: buildWhen,
        ),
      );

  void listener(BuildContext context, CurrencyPageState state) {
    if (state is NavigateToExchangePageCurrencyPageState) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              ExchangePage(state.currency.name, double.parse(state.currency.usdValue)),
        ),
      );
    } else if (state is InitialCurrencyPageState) {
      widget.cubit.loadData();
    }
  }

  Widget builder(BuildContext context, CurrencyPageState state) {
    if (state is LoadedCurrencyPageState) {
      final items = state.uiModel.currencies;
      return Scaffold(
        appBar: AppBar(),
        body: RefreshIndicator(
          onRefresh: () async {
            await widget.cubit.loadData();
          },
          child: ListView.builder(
            itemCount: items.length,
            itemBuilder: (context, index) => ListTile(
              title: Text(items[index].name),
              subtitle: Text(items[index].usdValue),
              onTap: () {
                widget.cubit.onLoadExchangePage(items[index]);
              },
            ),
          ),
        ),
      );
    } else if (state is ErrorCurrencyPageState) {
      return Scaffold(
        body: Container(
          color: Colors.red,
        ),
      );
    } else {
      return const Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
  }

  bool buildWhen(
      CurrencyPageState previousState, CurrencyPageState currentState) {
    if (currentState is NavigateToExchangePageCurrencyPageState) {
      return false;
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    widget.cubit.loadData();
  }

  @override
  void deactivate() {
    widget.cubit.close();
    super.deactivate();
  }
}
