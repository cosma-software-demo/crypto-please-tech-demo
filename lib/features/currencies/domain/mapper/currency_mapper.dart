import 'package:cryptoplease_tech_demo/features/currencies/data/model/currency_model.dart';
import 'package:cryptoplease_tech_demo/features/currencies/domain/entity/currency.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class CurrencyMapper {
  List<Currency> toCurrencyList(List<CurrencyModel> models) =>
      models.map((model) => toCurrency(model)).toList();

  Currency toCurrency(CurrencyModel model) => Currency.fact(
        model.name,
        model.market_data.current_price.usd.toString(),
      );
}
