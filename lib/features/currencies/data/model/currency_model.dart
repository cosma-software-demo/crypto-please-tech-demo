import 'package:cryptoplease_tech_demo/core/decode_exception.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'currency_model.g.dart';

part 'currency_model.freezed.dart';

@freezed
class CurrencyModel with _$CurrencyModel {
  factory CurrencyModel.fact(String name, MarketData market_data) =
      _CurrencyModel;

  factory CurrencyModel.fromJson(Map<String, dynamic> json) =>
      _$CurrencyModelFromJson(json);
}

@freezed
class MarketData with _$MarketData {
  factory MarketData.fact(CurrentPrice current_price) = _MarketData;

  factory MarketData.fromJson(Map<String, dynamic> json) =>
      _$MarketDataFromJson(json);
}

@freezed
class CurrentPrice with _$CurrentPrice {
  factory CurrentPrice.fact(num usd) = _CurrentPrice;

  factory CurrentPrice.fromJson(Map<String, dynamic> json) =>
      _$CurrentPriceFromJson(json);
}
