// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'currency_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CurrencyModel _$CurrencyModelFromJson(Map<String, dynamic> json) {
  return _CurrencyModel.fromJson(json);
}

/// @nodoc
class _$CurrencyModelTearOff {
  const _$CurrencyModelTearOff();

  _CurrencyModel fact(String name, MarketData market_data) {
    return _CurrencyModel(
      name,
      market_data,
    );
  }

  CurrencyModel fromJson(Map<String, Object?> json) {
    return CurrencyModel.fromJson(json);
  }
}

/// @nodoc
const $CurrencyModel = _$CurrencyModelTearOff();

/// @nodoc
mixin _$CurrencyModel {
  String get name => throw _privateConstructorUsedError;
  MarketData get market_data => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String name, MarketData market_data) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String name, MarketData market_data)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String name, MarketData market_data)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrencyModel value) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrencyModel value)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrencyModel value)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrencyModelCopyWith<CurrencyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrencyModelCopyWith<$Res> {
  factory $CurrencyModelCopyWith(
          CurrencyModel value, $Res Function(CurrencyModel) then) =
      _$CurrencyModelCopyWithImpl<$Res>;
  $Res call({String name, MarketData market_data});

  $MarketDataCopyWith<$Res> get market_data;
}

/// @nodoc
class _$CurrencyModelCopyWithImpl<$Res>
    implements $CurrencyModelCopyWith<$Res> {
  _$CurrencyModelCopyWithImpl(this._value, this._then);

  final CurrencyModel _value;
  // ignore: unused_field
  final $Res Function(CurrencyModel) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? market_data = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      market_data: market_data == freezed
          ? _value.market_data
          : market_data // ignore: cast_nullable_to_non_nullable
              as MarketData,
    ));
  }

  @override
  $MarketDataCopyWith<$Res> get market_data {
    return $MarketDataCopyWith<$Res>(_value.market_data, (value) {
      return _then(_value.copyWith(market_data: value));
    });
  }
}

/// @nodoc
abstract class _$CurrencyModelCopyWith<$Res>
    implements $CurrencyModelCopyWith<$Res> {
  factory _$CurrencyModelCopyWith(
          _CurrencyModel value, $Res Function(_CurrencyModel) then) =
      __$CurrencyModelCopyWithImpl<$Res>;
  @override
  $Res call({String name, MarketData market_data});

  @override
  $MarketDataCopyWith<$Res> get market_data;
}

/// @nodoc
class __$CurrencyModelCopyWithImpl<$Res>
    extends _$CurrencyModelCopyWithImpl<$Res>
    implements _$CurrencyModelCopyWith<$Res> {
  __$CurrencyModelCopyWithImpl(
      _CurrencyModel _value, $Res Function(_CurrencyModel) _then)
      : super(_value, (v) => _then(v as _CurrencyModel));

  @override
  _CurrencyModel get _value => super._value as _CurrencyModel;

  @override
  $Res call({
    Object? name = freezed,
    Object? market_data = freezed,
  }) {
    return _then(_CurrencyModel(
      name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      market_data == freezed
          ? _value.market_data
          : market_data // ignore: cast_nullable_to_non_nullable
              as MarketData,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CurrencyModel implements _CurrencyModel {
  _$_CurrencyModel(this.name, this.market_data);

  factory _$_CurrencyModel.fromJson(Map<String, dynamic> json) =>
      _$$_CurrencyModelFromJson(json);

  @override
  final String name;
  @override
  final MarketData market_data;

  @override
  String toString() {
    return 'CurrencyModel.fact(name: $name, market_data: $market_data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CurrencyModel &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality()
                .equals(other.market_data, market_data));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(market_data));

  @JsonKey(ignore: true)
  @override
  _$CurrencyModelCopyWith<_CurrencyModel> get copyWith =>
      __$CurrencyModelCopyWithImpl<_CurrencyModel>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String name, MarketData market_data) fact,
  }) {
    return fact(name, market_data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String name, MarketData market_data)? fact,
  }) {
    return fact?.call(name, market_data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String name, MarketData market_data)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(name, market_data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrencyModel value) fact,
  }) {
    return fact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrencyModel value)? fact,
  }) {
    return fact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrencyModel value)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrencyModelToJson(this);
  }
}

abstract class _CurrencyModel implements CurrencyModel {
  factory _CurrencyModel(String name, MarketData market_data) =
      _$_CurrencyModel;

  factory _CurrencyModel.fromJson(Map<String, dynamic> json) =
      _$_CurrencyModel.fromJson;

  @override
  String get name;
  @override
  MarketData get market_data;
  @override
  @JsonKey(ignore: true)
  _$CurrencyModelCopyWith<_CurrencyModel> get copyWith =>
      throw _privateConstructorUsedError;
}

MarketData _$MarketDataFromJson(Map<String, dynamic> json) {
  return _MarketData.fromJson(json);
}

/// @nodoc
class _$MarketDataTearOff {
  const _$MarketDataTearOff();

  _MarketData fact(CurrentPrice current_price) {
    return _MarketData(
      current_price,
    );
  }

  MarketData fromJson(Map<String, Object?> json) {
    return MarketData.fromJson(json);
  }
}

/// @nodoc
const $MarketData = _$MarketDataTearOff();

/// @nodoc
mixin _$MarketData {
  CurrentPrice get current_price => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CurrentPrice current_price) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CurrentPrice current_price)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CurrentPrice current_price)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_MarketData value) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_MarketData value)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_MarketData value)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MarketDataCopyWith<MarketData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MarketDataCopyWith<$Res> {
  factory $MarketDataCopyWith(
          MarketData value, $Res Function(MarketData) then) =
      _$MarketDataCopyWithImpl<$Res>;
  $Res call({CurrentPrice current_price});

  $CurrentPriceCopyWith<$Res> get current_price;
}

/// @nodoc
class _$MarketDataCopyWithImpl<$Res> implements $MarketDataCopyWith<$Res> {
  _$MarketDataCopyWithImpl(this._value, this._then);

  final MarketData _value;
  // ignore: unused_field
  final $Res Function(MarketData) _then;

  @override
  $Res call({
    Object? current_price = freezed,
  }) {
    return _then(_value.copyWith(
      current_price: current_price == freezed
          ? _value.current_price
          : current_price // ignore: cast_nullable_to_non_nullable
              as CurrentPrice,
    ));
  }

  @override
  $CurrentPriceCopyWith<$Res> get current_price {
    return $CurrentPriceCopyWith<$Res>(_value.current_price, (value) {
      return _then(_value.copyWith(current_price: value));
    });
  }
}

/// @nodoc
abstract class _$MarketDataCopyWith<$Res> implements $MarketDataCopyWith<$Res> {
  factory _$MarketDataCopyWith(
          _MarketData value, $Res Function(_MarketData) then) =
      __$MarketDataCopyWithImpl<$Res>;
  @override
  $Res call({CurrentPrice current_price});

  @override
  $CurrentPriceCopyWith<$Res> get current_price;
}

/// @nodoc
class __$MarketDataCopyWithImpl<$Res> extends _$MarketDataCopyWithImpl<$Res>
    implements _$MarketDataCopyWith<$Res> {
  __$MarketDataCopyWithImpl(
      _MarketData _value, $Res Function(_MarketData) _then)
      : super(_value, (v) => _then(v as _MarketData));

  @override
  _MarketData get _value => super._value as _MarketData;

  @override
  $Res call({
    Object? current_price = freezed,
  }) {
    return _then(_MarketData(
      current_price == freezed
          ? _value.current_price
          : current_price // ignore: cast_nullable_to_non_nullable
              as CurrentPrice,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MarketData implements _MarketData {
  _$_MarketData(this.current_price);

  factory _$_MarketData.fromJson(Map<String, dynamic> json) =>
      _$$_MarketDataFromJson(json);

  @override
  final CurrentPrice current_price;

  @override
  String toString() {
    return 'MarketData.fact(current_price: $current_price)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MarketData &&
            const DeepCollectionEquality()
                .equals(other.current_price, current_price));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(current_price));

  @JsonKey(ignore: true)
  @override
  _$MarketDataCopyWith<_MarketData> get copyWith =>
      __$MarketDataCopyWithImpl<_MarketData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(CurrentPrice current_price) fact,
  }) {
    return fact(current_price);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(CurrentPrice current_price)? fact,
  }) {
    return fact?.call(current_price);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(CurrentPrice current_price)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(current_price);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_MarketData value) fact,
  }) {
    return fact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_MarketData value)? fact,
  }) {
    return fact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_MarketData value)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_MarketDataToJson(this);
  }
}

abstract class _MarketData implements MarketData {
  factory _MarketData(CurrentPrice current_price) = _$_MarketData;

  factory _MarketData.fromJson(Map<String, dynamic> json) =
      _$_MarketData.fromJson;

  @override
  CurrentPrice get current_price;
  @override
  @JsonKey(ignore: true)
  _$MarketDataCopyWith<_MarketData> get copyWith =>
      throw _privateConstructorUsedError;
}

CurrentPrice _$CurrentPriceFromJson(Map<String, dynamic> json) {
  return _CurrentPrice.fromJson(json);
}

/// @nodoc
class _$CurrentPriceTearOff {
  const _$CurrentPriceTearOff();

  _CurrentPrice fact(num usd) {
    return _CurrentPrice(
      usd,
    );
  }

  CurrentPrice fromJson(Map<String, Object?> json) {
    return CurrentPrice.fromJson(json);
  }
}

/// @nodoc
const $CurrentPrice = _$CurrentPriceTearOff();

/// @nodoc
mixin _$CurrentPrice {
  num get usd => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(num usd) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(num usd)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(num usd)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrentPrice value) fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrentPrice value)? fact,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrentPrice value)? fact,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CurrentPriceCopyWith<CurrentPrice> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CurrentPriceCopyWith<$Res> {
  factory $CurrentPriceCopyWith(
          CurrentPrice value, $Res Function(CurrentPrice) then) =
      _$CurrentPriceCopyWithImpl<$Res>;
  $Res call({num usd});
}

/// @nodoc
class _$CurrentPriceCopyWithImpl<$Res> implements $CurrentPriceCopyWith<$Res> {
  _$CurrentPriceCopyWithImpl(this._value, this._then);

  final CurrentPrice _value;
  // ignore: unused_field
  final $Res Function(CurrentPrice) _then;

  @override
  $Res call({
    Object? usd = freezed,
  }) {
    return _then(_value.copyWith(
      usd: usd == freezed
          ? _value.usd
          : usd // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
abstract class _$CurrentPriceCopyWith<$Res>
    implements $CurrentPriceCopyWith<$Res> {
  factory _$CurrentPriceCopyWith(
          _CurrentPrice value, $Res Function(_CurrentPrice) then) =
      __$CurrentPriceCopyWithImpl<$Res>;
  @override
  $Res call({num usd});
}

/// @nodoc
class __$CurrentPriceCopyWithImpl<$Res> extends _$CurrentPriceCopyWithImpl<$Res>
    implements _$CurrentPriceCopyWith<$Res> {
  __$CurrentPriceCopyWithImpl(
      _CurrentPrice _value, $Res Function(_CurrentPrice) _then)
      : super(_value, (v) => _then(v as _CurrentPrice));

  @override
  _CurrentPrice get _value => super._value as _CurrentPrice;

  @override
  $Res call({
    Object? usd = freezed,
  }) {
    return _then(_CurrentPrice(
      usd == freezed
          ? _value.usd
          : usd // ignore: cast_nullable_to_non_nullable
              as num,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CurrentPrice implements _CurrentPrice {
  _$_CurrentPrice(this.usd);

  factory _$_CurrentPrice.fromJson(Map<String, dynamic> json) =>
      _$$_CurrentPriceFromJson(json);

  @override
  final num usd;

  @override
  String toString() {
    return 'CurrentPrice.fact(usd: $usd)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CurrentPrice &&
            const DeepCollectionEquality().equals(other.usd, usd));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(usd));

  @JsonKey(ignore: true)
  @override
  _$CurrentPriceCopyWith<_CurrentPrice> get copyWith =>
      __$CurrentPriceCopyWithImpl<_CurrentPrice>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(num usd) fact,
  }) {
    return fact(usd);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(num usd)? fact,
  }) {
    return fact?.call(usd);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(num usd)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(usd);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_CurrentPrice value) fact,
  }) {
    return fact(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_CurrentPrice value)? fact,
  }) {
    return fact?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_CurrentPrice value)? fact,
    required TResult orElse(),
  }) {
    if (fact != null) {
      return fact(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$_CurrentPriceToJson(this);
  }
}

abstract class _CurrentPrice implements CurrentPrice {
  factory _CurrentPrice(num usd) = _$_CurrentPrice;

  factory _CurrentPrice.fromJson(Map<String, dynamic> json) =
      _$_CurrentPrice.fromJson;

  @override
  num get usd;
  @override
  @JsonKey(ignore: true)
  _$CurrentPriceCopyWith<_CurrentPrice> get copyWith =>
      throw _privateConstructorUsedError;
}
