// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currency_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CurrencyModel _$$_CurrencyModelFromJson(Map<String, dynamic> json) =>
    _$_CurrencyModel(
      json['name'] as String,
      MarketData.fromJson(json['market_data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CurrencyModelToJson(_$_CurrencyModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'market_data': instance.market_data,
    };

_$_MarketData _$$_MarketDataFromJson(Map<String, dynamic> json) =>
    _$_MarketData(
      CurrentPrice.fromJson(json['current_price'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_MarketDataToJson(_$_MarketData instance) =>
    <String, dynamic>{
      'current_price': instance.current_price,
    };

_$_CurrentPrice _$$_CurrentPriceFromJson(Map<String, dynamic> json) =>
    _$_CurrentPrice(
      json['usd'] as num,
    );

Map<String, dynamic> _$$_CurrentPriceToJson(_$_CurrentPrice instance) =>
    <String, dynamic>{
      'usd': instance.usd,
    };
