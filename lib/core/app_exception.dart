class AppException implements Exception {
  final dynamic e;
  final StackTrace? stackTrace;

  AppException(this.e, this.stackTrace);
}
