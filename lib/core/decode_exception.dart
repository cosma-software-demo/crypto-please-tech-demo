class DecodeException implements Exception {
  final dynamic e;
  final StackTrace? stackTrace;

  DecodeException(this.e, this.stackTrace);
}
